#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <iomanip>     
#include <cstring>
#include <ctime>
#include <climits>
#include <exception>

#include "common.hpp"
#include "lodepng.h"
#include "histogram.hpp"

namespace IMAC
{
	// Main function
	void main(int argc, char **argv) 
	{
		std::cout << "Histogram equalization" << std::endl;

		/* ---------- LOAD IMAGE ---------- */

		// Get input image
		const std::string fileName = "./landscape.png";
		const std::string outputName = "landscape_GPU.png";
		std::vector<uchar> inputUchar;
		uint imgWidth;
		uint imgHeight;

		std::cout << "Loading " << fileName << std::endl;
		unsigned error = lodepng::decode(inputUchar, imgWidth, imgHeight, fileName, LCT_RGBA);
		if (error)
		{
			throw std::runtime_error("Error loadpng::decode: " + std::string(lodepng_error_text(error)));
		}
		// Convert to uchar3 for exercise convenience
		std::vector<uchar3> inputImage;
		inputImage.resize(inputUchar.size() / 4);
		for (uint i = 0; i < inputImage.size(); ++i)
		{
			const uint id = 4 * i;
			inputImage[i].x = inputUchar[id];
			inputImage[i].y = inputUchar[id + 1];
			inputImage[i].z = inputUchar[id + 2];
		}
		inputUchar.clear();
		std::cout << "Image has " << imgWidth << " x " << imgHeight << " pixels (RGB)" << std::endl << std::endl;

		/* ---------- EXECUTION ---------- */

		std::vector<uchar3> outputGPU(imgWidth * imgHeight);
		
		histogramGPU(inputImage, imgWidth, imgHeight, outputGPU);

		/* ---------- EXPORT RESULT ---------- */

		std::cout << "Save image as: " << outputName << std::endl;
		error = lodepng::encode(outputName, reinterpret_cast<uchar *>(outputGPU.data()), imgWidth, imgHeight, LCT_RGB);
		if (error)
		{
			throw std::runtime_error("Error loadpng::decode: " + std::string(lodepng_error_text(error)));
		}
	}
}

int main(int argc, char **argv) 
{
	try
	{
		IMAC::main(argc, argv);
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << std::endl;
	}
}
