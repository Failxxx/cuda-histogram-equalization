#include "histogram.hpp"
#include "common.hpp"

__device__ uchar3 convert_one_pixel_to_hsv(uchar3 pixel) {
	float fR, fG, fB;
	float fH, fS, fV;
	fR = pixel.x * 0.003921569F; fG = pixel.y * 0.003921569F; fB = pixel.z * 0.003921569F;

	float fCMax = fmaxf(fmaxf(fR, fG), fB);
	float fCMin = fminf(fminf(fR, fG), fB);
	float fDelta = fCMax - fCMin;
	
	if(fDelta > 0) {
	  if(fCMax == fR) {
		fH = 60 * (fmodf(((fG - fB) / fDelta), 6));
	  } else if(fCMax == fG) {
		fH = 60 * (((fB - fR) / fDelta) + 2);
	  } else if(fCMax == fB) {
		fH = 60 * (((fR - fG) / fDelta) + 4);
	  }
	  
	  if(fCMax > 0) {
		fS = fDelta / fCMax;
	  } else {
		fS = 0;
	  }
	  
	  fV = fCMax;
	} else {
	  fH = 0;
	  fS = 0;
	  fV = fCMax;
	}
	
	if(fH < 0) {
	  fH = 360 + fH;
	}

	return (uchar3) {fH * 255.0f / 360.0f, fS * 255.0f, fV * 255.0f};
}

__device__ uchar3 convert_one_pixel_to_rgb(uchar3 pixel) {
	float fH, fS, fV;
	float fR, fG, fB;
	fH = pixel.x * 0.003921569F * 360.0f; fS = pixel.y * 0.003921569F; fV = pixel.z * 0.003921569F;
	
	float fC = fV * fS; // Chroma
	float fHPrime = fmodf(fH / 60.0, 6);
	float fX = fC * (1 - fabsf(fmodf(fHPrime, 2) - 1));
	float fM = fV - fC;
	
	if(0 <= fHPrime && fHPrime < 1) {
	  fR = fC;
	  fG = fX;
	  fB = 0;
	} else if(1 <= fHPrime && fHPrime < 2) {
	  fR = fX;
	  fG = fC;
	  fB = 0;
	} else if(2 <= fHPrime && fHPrime < 3) {
	  fR = 0;
	  fG = fC;
	  fB = fX;
	} else if(3 <= fHPrime && fHPrime < 4) {
	  fR = 0;
	  fG = fX;
	  fB = fC;
	} else if(4 <= fHPrime && fHPrime < 5) {
	  fR = fX;
	  fG = 0;
	  fB = fC;
	} else if(5 <= fHPrime && fHPrime < 6) {
	  fR = fC;
	  fG = 0;
	  fB = fX;
	} else {
	  fR = 0;
	  fG = 0;
	  fB = 0;
	}
	
	fR += fM;
	fG += fM;
	fB += fM;

	return (uchar3) {fR * 255.0f, fG * 255.0f, fB * 255.0f};
}

namespace IMAC
{
	__global__ void rgb_to_hsv(const uchar3* const input, const int imgWidth, const int imgHeight, uchar3* const output) {
		int column = threadIdx.x + blockIdx.x * blockDim.x;
		int row = threadIdx.y + blockIdx.y * blockDim.y;
		int id = (column + row * imgWidth);
		
		if(row < imgHeight && column < imgWidth) {
			output[id] = convert_one_pixel_to_hsv(input[id]);
		}
	}

	__global__ void hsv_to_rgb(const uchar3* const input, const int imgWidth, const int imgHeight, uchar3* const output) {
		int column = threadIdx.x + blockIdx.x * blockDim.x;
		int row = threadIdx.y + blockIdx.y * blockDim.y;
		int id = (column + row * imgWidth);
		
		if(row < imgHeight && column < imgWidth) {
			output[id] = convert_one_pixel_to_rgb(input[id]);
		}
	}

	__global__ void compute_histogram(const uchar3* const input, const int imgWidth, const int imgHeight, int* const dev_histogram) {
		int column = threadIdx.x + blockIdx.x * blockDim.x;
		int row = threadIdx.y + blockIdx.y * blockDim.y;
		int id = (column + row * imgWidth);

		if(row < imgHeight && column < imgWidth) {
			const int value = input[id].z; // input[id].z = value of V (in HSV)
			atomicAdd(&dev_histogram[value], 1);
		}
	}

	__global__ void compute_repartition(const int* const dev_histogram, int* const dev_result) {
		int id = threadIdx.x;
		int result = 0;

		if(id < 256) {
			for(int i = 0; i < id; ++i) {
				result += dev_histogram[i];
			}

			dev_result[id] = result;
		}
	}

	__global__ void equalize_histogram(const uchar3* const input, const int imgWidth, const int imgHeight, const int* const r, uchar3* const output) {
		int column = threadIdx.x + blockIdx.x * blockDim.x;
		int row = threadIdx.y + blockIdx.y * blockDim.y;
		int id = (column + row * imgWidth);

		const float n = imgWidth * imgHeight;
		const float L = 256;

		if(row < imgHeight && column < imgWidth) {
			output[id] = input[id];
			const float factor = r[input[id].z] * (L - 1) / (L * n);
			output[id].z = (uchar)max(0.0f , min(factor * 255, 255.0f));
		}
	}


	void histogramGPU(const std::vector<uchar3> &input, const uint imgWidth, const uint imgHeight, std::vector<uchar3> &output)
    {
		ChronoGPU chrGPU;

		// Create arrays for the GPU
		uchar3* dev_input = NULL;
		int* dev_histogram = NULL;
		int* dev_repartition = NULL;
		uchar3* dev_hsv_image = NULL;
		uchar3* dev_hsv_equalized_image = NULL;
		uchar3* dev_rgb_image = NULL;
		
		// Allocate arrays on device
		int sizeImage = imgWidth * imgHeight;
		int bytesImage = sizeImage * sizeof(uchar3);
		cudaMalloc((void **) &dev_input, bytesImage);
		cudaMalloc((void **) &dev_hsv_image, bytesImage);
		cudaMalloc((void **) &dev_hsv_equalized_image, bytesImage);
		cudaMalloc((void **) &dev_rgb_image, bytesImage);

		// Histogram
		int sizeHistogram = 256;
		int bytesHistogram = sizeHistogram * sizeof(int);
		cudaMalloc((void **) &dev_histogram, bytesHistogram);
		// Repartition
		cudaMalloc((void **) &dev_repartition, bytesHistogram);

		// Copy data from host to device
		cudaMemcpy(dev_input, input.data(), bytesImage, cudaMemcpyHostToDevice);

		// Launch kernel
		chrGPU.start();

		float nbThreads = 8.f;
		std::cout << "Grid of blocks (dimensions) : {" << std::ceil(imgWidth / nbThreads) << ", " << std::ceil(imgHeight / nbThreads) << "}" << std::endl;
		std::cout << "Block of threads (dimensions) : {" << (int)nbThreads << ", " << (int)nbThreads << "}" << std::endl;
		const dim3 blocks  = dim3(std::ceil(imgWidth / nbThreads), std::ceil(imgHeight / nbThreads));
		const dim3 threads = dim3((int)nbThreads, (int)nbThreads);

		rgb_to_hsv<<< blocks , threads >>>(dev_input, imgWidth, imgHeight, dev_hsv_image);
		compute_histogram<<< blocks, threads >>>(dev_hsv_image, imgWidth, imgHeight, dev_histogram);
		compute_repartition<<< 1, 256, bytesHistogram >>>(dev_histogram, dev_repartition);
		equalize_histogram<<< blocks , threads >>>(dev_hsv_image, imgWidth, imgHeight, dev_repartition, dev_hsv_equalized_image);
		hsv_to_rgb<<< blocks , threads >>>(dev_hsv_equalized_image, imgWidth, imgHeight, dev_rgb_image);

		chrGPU.stop();
		std::cout << "Computing done: " << chrGPU.elapsedTime() << " ms" << std::endl << std::endl;

		// Copy data from device to host
		cudaMemcpy(output.data(), dev_rgb_image, bytesImage, cudaMemcpyDeviceToHost);

		// Free arrays on device
		cudaFree(dev_input);
		cudaFree(dev_histogram);
		cudaFree(dev_hsv_image);
		cudaFree(dev_hsv_equalized_image);
		cudaFree(dev_rgb_image);
    }

	void printTiming(const float2 timing)
	{
		std::cout << ( timing.x < 1.f ? 1e3f * timing.x : timing.x ) << " us on device and ";
		std::cout << ( timing.y < 1.f ? 1e3f * timing.y : timing.y ) << " us on host." << std::endl;
	}
}
